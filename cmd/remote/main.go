//go:generate protoc -I=../ --go_out=../ ../proto/data.proto
package main

import (
    "fmt"
    "log"
    "os"
    "os/signal"
    "roombot"
)

func main() {
    stickChannel, stickCancel := roombot.MonitorJoystick()
    streamCancel, err := roombot.StartStreaming(fmt.Sprintf("%s:10123", os.Args[1]), stickChannel)
    if err != nil {
        log.Fatalf("could not start streaming: %v", err)
    }

    sig := make(chan os.Signal)
    signal.Notify(sig, os.Kill, os.Interrupt)
    fmt.Println("Started. CTRL+C to exit")
    <-sig
    streamCancel()
    stickCancel()
}
