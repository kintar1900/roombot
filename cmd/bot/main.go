package main

import (
    "fmt"
    "os"
    "os/signal"
    "roombot"
)

const SeqCutoff = 1<<32 - 20

func main() {
    //scr, _ := tcell.NewScreen()
    //scr.Init()
    //scr.Sync()
    //defer scr.Fini()

    ch, cf := roombot.ReceiveData()
    sig := make(chan os.Signal)
    signal.Notify(sig, os.Kill, os.Interrupt)
    seq := uint32(0)
    for {
        select {
        case <-sig:
            cf()
            return
        case msg := <-ch:
            if msg.Sequence < seq && seq < SeqCutoff {
                continue
            }
            seq = msg.Sequence
            //roombot.Emit(scr, 0, 0, tcell.StyleDefault, fmt.Sprintf("drive data: %6d, %6d", msg.Drive.RightWheel, msg.Drive.LeftWheel))
            //scr.Show()
            fmt.Printf("%6d, %6d\n")
        }
    }
}
