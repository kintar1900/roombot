module roombot

go 1.15

require (
	github.com/gdamore/tcell/v2 v2.1.0
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/websocket v1.4.2
	github.com/mattn/go-runewidth v0.0.9
	github.com/rs/zerolog v1.20.0
	github.com/simulatedsimian/joystick v1.0.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	gobot.io/x/gobot v1.15.0
	gocv.io/x/gocv v0.26.0
	google.golang.org/grpc v1.34.1
	google.golang.org/protobuf v1.25.0
)
