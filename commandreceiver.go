package roombot

import (
    "context"
    "errors"
    "github.com/golang/protobuf/proto"
    "net"
    "os"
    "roombot/common/data"
)

func ReceiveData() (chan *data.CommandFrame, context.CancelFunc) {
    listener, err := net.ListenUDP("udp", &net.UDPAddr{
        IP: net.IPv4(0,0,0,0),
        Port: 10123,
    })

    if err != nil {
        panic(err)
    }

    dataChan := make(chan *data.CommandFrame)
    working := true
    cf := func() {
        working = false
    }

    inBuffer := make([]byte, 512)
    pbBuff := proto.NewBuffer(nil)

    for working {
        readLen, err := listener.Read(inBuffer)
        if err != nil {
            panic(err)
        }
        pbBuff.SetBuf(append(pbBuff.Unread(), inBuffer[:readLen]...))
        msg := data.CommandFrame{}
        err = pbBuff.DecodeMessage(&msg)
        if err != nil && errors.Is(err, os.ErrDeadlineExceeded) {
            continue
        }
        if err != nil {
            panic(err)
        }
        dataChan <- &msg
    }

    return dataChan, cf
}
