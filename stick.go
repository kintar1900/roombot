package roombot

import (
    "context"
    "fmt"
    "github.com/rs/zerolog/log"
    "github.com/simulatedsimian/joystick"
    "time"
)

type StickData [4]int16

const(
    deadZoneMax = 1500
    axisMax = 32767
)

// MonitorJoystick starts a goroutine to monitor the state of the first joystick
// It attempts to report on the stick's axes every 15ms and return their data to the channel returned by this function.
// The context.CancelFunc returned can be called to stop monitoring
func MonitorJoystick() (<-chan StickData, context.CancelFunc) {
    js, err := joystick.Open(0)
    if err != nil {
        panic(fmt.Sprintf("could not find joystick: %v\n", err))
    }
    working := true

    cf := func() {
        working = false
    }

    dataChan := make(chan StickData)
    ticker := time.NewTicker(time.Millisecond * 15)

    go func() {
        defer js.Close()

        for working {
            select {
            case <-ticker.C:
                state, err := js.Read()
                if err != nil {
                    log.Printf("failed to read joystick state: %v\n", err)
                }
                dataChan <- StickData{
                    int16(state.AxisData[0]),
                    int16(state.AxisData[1]),
                    int16(state.AxisData[3]),
                    int16(state.AxisData[4]),
                }
            }
        }
    }()

    return dataChan, cf
}
