package roombot

import "github.com/gdamore/tcell/v2"

func Emit(s tcell.Screen, x, y int, style tcell.Style, str string) {
    for i, r := range str {
        s.SetContent(x + i, y, r, nil, style)
    }
}
