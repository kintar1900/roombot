package roombot

import (
    "context"
    "github.com/golang/protobuf/proto"
    "net"
    "roombot/common/data"
)

func StartStreaming(address string, stickData <-chan StickData) (context.CancelFunc, error) {
    c, err := net.Dial("udp", address)
    if err != nil {
        return nil, err
    }

    working := true

    cf := func() {
        working = false
    }

    go func() {
        seq := uint32(0)
        buf := proto.NewBuffer(nil)
        for working {
            select {
            case sd := <-stickData:
                cf := &data.CommandFrame{
                    Sequence: seq,
                    Drive: &data.DriveControl{
                        LeftWheel:  int32(sd[1]),
                        RightWheel: int32(sd[3]),
                    },
                }
                err := buf.EncodeMessage(cf)
                if err != nil {
                    panic(err)
                }
                _, err = c.Write(buf.Bytes())
                if err != nil {
                    panic(err)
                }
                buf.Reset()
            }
        }
    }()

    return cf, nil
}
